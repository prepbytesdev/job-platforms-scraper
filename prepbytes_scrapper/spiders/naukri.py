import json
import scrapy
from bs4 import BeautifulSoup
from ..items import JobItem


class NaukriSpider(scrapy.Spider):
    handle_httpstatus_list = [400, 401]
    name = "naukri"
    start_urls = ["https://www.naukri.com/software-developer-jobs"]
    api_url = "https://www.naukri.com/jobapi/v3/search"
    base_url = "https://www.naukri.com"
    page_no = 0
    page_limit = 100
    no_of_results = 500
    headers = {
        "authority": "www.naukri.com",
        "pragma": "no-cache",
        "cache-control": "no-cache",
        "sec-ch-ua": '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        "sec-ch-ua-mobile": "?0",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
        "systemid": "109",
        "content-type": "application/json",
        "accept": "application/json",
        "clientid": "d3skt0p",
        "appid": "109",
        "gid": "LOCATION,INDUSTRY",
        "sec-fetch-site": "same-origin",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "referer": "https://www.naukri.com/jobs-in-india",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
    }

    def start_requests(self):
        for url in self.start_urls:
            yield scrapy.Request(url=url, callback=self.set_cookies)

    def config_url(self):
        return f"{self.api_url}?noOfResults={self.no_of_results}&urlType=search_by_location&searchType=adv&location=india&pageNo={self.page_no}&sort=r&seoKey=jobs-in-india&src=jobsearchDesk&latLong=&sid=16228181029404980_7"

    def set_cookies(self, response):
        print("Cookies Set...")

        url = self.config_url()

        request = scrapy.Request(
            url=url,
            method="GET",
            dont_filter=True,
            headers=self.headers,
            callback=self.parse_api,
        )

        yield request

    def parse_api(self, response):
        print(f"Fetching Page: {response.url}...")
        raw_data = response.body
        data = json.loads(raw_data)
        if not "jobDetails" in data.keys():
            print({"return": "Last Page reached."})
            return
        elif self.page_no > self.page_limit:
            print({"return": "Page limit reached Returning."})
            return
        else:
            jobs = data["jobDetails"]

            spider_item = JobItem()
            print("len: ", len(jobs))

            for job in jobs:
                spider_item["source"] = "www.naukri.com"
                spider_item["title"] = job["title"]
                spider_item["company"] = job["companyName"]

                if "day" in job["footerPlaceholderLabel"].lower():
                    posting_time = job["footerPlaceholderLabel"]
                else:
                    posting_time = None
                spider_item["posting_time"] = posting_time

                spider_item["apply_by"] = None
                spider_item["duration"] = None

                for item in job["placeholders"]:
                    if item["type"] == "experience":
                        spider_item["experience"] = item["label"]
                    elif item["type"] == "salary":
                        spider_item["salary"] = item["label"]
                    elif item["type"] == "location":
                        spider_item["location"] = item["label"]

                spider_item["link"] = self.base_url + job["jdURL"]
                spider_item["description"] = job["jobDescription"]

                soup = BeautifulSoup(job["jobDescription"])
                spider_item["description_text"] = soup.get_text("\n")

                yield spider_item

            self.page_no += 1
            url = self.config_url()

            request = scrapy.Request(
                url=url,
                method="GET",
                dont_filter=True,
                headers=self.headers,
                callback=self.parse_api,
            )

            yield request
