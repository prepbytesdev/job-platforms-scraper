import scrapy
from bs4 import BeautifulSoup
from ..items import JobItem


class AngelSpider(scrapy.Spider):
    handle_httpstatus_list = [400, 401, 503]
    name = "internshala"
    start_urls = ["https://internshala.com/internships/engineering-internship/"]
    base_url = "https://internshala.com/"
    page_no = 1
    limit = 100
    last_page = None

    def start_requests(self):
        for parse_url in self.start_urls:
            request = scrapy.Request(
                url=self.config_url(parse_url), callback=self.parse
            )
            request.cb_kwargs["parse_url"] = parse_url
            yield request

    def config_url(self, url):
        return f"{url}page-{self.page_no}"

    def parse(self, response, parse_url):
        intern_url = (
            response.css("#internship_list_container")
            .css(".individual_internship")
            .css("a.view_detail_button::attr(href)")
            .getall()
        )

        if intern_url == None:
            print("No internship found. abort...")
            return

        print(f"Parsing Page {self.page_no} with Total items {len(intern_url)}...")

        curr_page = int(response.css("#pageNumber::text").get())

        if self.last_page == None:
            last_page = response.css("#total_pages::text").get()
            self.last_page = None if last_page == None else int(last_page)

        if curr_page == None or self.last_page == None:
            print("Pagination info not found. abort...")
            return

        for url in intern_url:
            yield scrapy.Request(
                url=f"{self.base_url}{url}", callback=self.parse_intern_url
            )

        if self.last_page <= curr_page or self.limit <= curr_page:
            print("Last Page...")
            return
        else:
            self.page_no += 1
            request = scrapy.Request(
                url=self.config_url(parse_url), callback=self.parse
            )
            request.cb_kwargs["parse_url"] = parse_url
            yield request

    def parse_intern_url(self, response):
        spider_item = JobItem()
        spider_item["source"] = "www.internshala.com"

        title = response.css(".heading_title::text").get().strip()
        spider_item["title"] = None if title == None else title.strip()

        company = response.css(".link_display_like_text::text").get().strip()
        spider_item["company"] = None if company is None else company.strip()

        spider_item["posting_time"] = None

        apply_by = response.css(".apply_by .item_body::text").get()
        spider_item["apply_by"] = None if apply_by == None else apply_by.strip()

        spider_item["duration"] = None
        other_details = (
            response.css(".other_detail_item").css(".item_body::text").getall()
        )
        for detail in other_details:
            test_detail = detail.strip()
            if test_detail != None and (
                "month" in test_detail or "year" in test_detail
            ):
                spider_item["duration"] = None
                break

        spider_item["experience"] = None

        salary = response.css(".stipend::text").get()
        spider_item["salary"] = None if salary == None else salary.strip()

        location = response.css(".location_link::text").get()
        spider_item["location"] = None if location == None else location.strip()

        spider_item["link"] = response.url
        print(f"Extracted {title} from page: {self.page_no}")
        description = response.css(".internship_details").get()
        soup = BeautifulSoup(description)
        spider_item["description"] = soup
        spider_item["description_text"] = soup.get_text()

        yield spider_item
