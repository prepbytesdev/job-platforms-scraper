import scrapy
from scrapy_splash import SplashRequest, SplashFormRequest
from scrapy.utils.response import open_in_browser
from ..items import JobItem


class GlassdoorSpider(scrapy.Spider):
    handle_httpstatus_list = [400, 401, 404]
    name = "glassdoor"
    start_urls = ["https://www.glassdoor.co.in/profile/login_input.htm"]
    job_urls = ["https://www.glassdoor.co.in/Job/software-developer-jobs-SRCH_KO0,18"]
    glassdoor_url = "https://www.glassdoor.com"
    page_no = 1

    def start_requests(self):
        for start_url in self.start_urls:
            yield SplashRequest(url=start_url, callback=self.login)

    def config_url(self, job_url):
        if self.page_no == 1:
            return f"{job_url}.htm"
        else:
            return f"{job_url}_IP{self.page_no}.htm"

    def login(self, response):
        print("Start login...")
        gdToken = response.xpath('//input[@name="gdToken"]/@value').get()
        print(gdToken)
        return SplashFormRequest.from_response(
            response,
            formdata={
                "gdToken": gdToken,
                "username": "avinaash.naagar@prepbytes.com",
                "password": "Pdnejoh.1urkqsrk",
            },
            callback=self.parse,
        )

    def parse(self, response):
        print("Login done...")
        for job_url in self.job_urls:
            url = self.config_url(job_url)
            print(url)
            request = response.follow(url, callback=self.parse_job_list)
            yield request

    def parse_job_list(self, response):
        print("Parsing job_list...")
        open_in_browser(response)
        job_item = response.css(".e1rrn5ka4")
        print(f"Found {len(job_item)} items...")
        for job in job_item:

            spider_item = JobItem()
            spider_item["source"] = self.glassdoor_url
            spider_item["title"] = (
                job.css("a.jobLink[data-test='job-link']").css("span::text").get()
            )
            spider_item["company"] = job.css("a.jobLink span::text").get()

            age = job.css("div[data-test='job-age']::text").get()
            spider_item["posting_time"] = None if age == None else age + " ago"

            spider_item["experience"] = None

            salary = job.css("span[data-test='detailSalary']::text").get()
            salary_estimated_by = job.css(
                "span[data-test='detailSalary'] span::text"
            ).get()
            if salary == None:
                spider_item["salary"] = None
            else:
                salary_rpad = (
                    "" if salary_estimated_by == None else " " + salary_estimated_by
                )
                spider_item["salary"] = salary + salary_rpad

            spider_item["location"] = job.css(".e1rrn5ka0::text").get()

            link = job.css("a.jobLink::attr(href)").get()
            spider_item["link"] = None if link == None else self.glassdoor_url + link

            yield spider_item

        next_page_item = response.css("#FooterPageNav").css("li.e1gri00l3").get()
        print(next_page_item)


# https://www.glassdoor.co.in/Job/software-developer-jobs-SRCH_KO0,18_IP4.htm
# https://www.glassdoor.co.in/Job/software-developer-jobs-SRCH_KO0,18_IP30.htm
