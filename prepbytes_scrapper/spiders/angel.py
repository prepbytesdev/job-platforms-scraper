import scrapy


class AngelSpider(scrapy.Spider):
    handle_httpstatus_list = [400, 401]
    name = "angel"
    start_urls = ["https://internshala.com/internships/engineering-internship/"]
    page_no = 1

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url=url, callback=self.parse)

    def config_url(self):
        return f"{self.start_urls}page-{self.page_no}"

    def parse(self, response):
        print()
        pass
