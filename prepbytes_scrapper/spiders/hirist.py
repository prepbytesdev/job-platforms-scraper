import json
import datetime
import scrapy
from scrapy_splash import SplashRequest
from ..items import JobItem


class HiristSpider(scrapy.Spider):
    handle_httpstatus_list = [400, 401, 404]
    name = "hirist"
    start_urls = ["https://www.hirist.com/"]
    api_urls = [
        "https://jobseeker-api.hirist.com/jobfeed/-1/cat/1",
        "https://jobseeker-api.hirist.com/jobfeed/-1/cat/2",
    ]
    page_no = 0
    headers = {
        "authority": "jobseeker-api.hirist.com",
        "sec-ch-ua": '" Not A;Brand";v="99", "Chromium";v="90", "Google Chrome";v="90"',
        "accept": "application/json, text/plain, */*",
        "authorization": "Bearer undefined",
        "sec-ch-ua-mobile": "?0",
        "user-agent": "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/90.0.4430.212 Safari/537.36",
        "origin": "https://www.hirist.com",
        "sec-fetch-site": "same-site",
        "sec-fetch-mode": "cors",
        "sec-fetch-dest": "empty",
        "referer": "https://www.hirist.com/",
        "accept-language": "en-GB,en-US;q=0.9,en;q=0.8",
    }

    def start_requests(self):
        for url in self.start_urls:
            yield SplashRequest(url=url, callback=self.set_cookies)

    def config_url(self, api_url):
        return f"{api_url}?pageNo={self.page_no}&loc=&minexp=0&maxexp=0"

    def set_cookies(self, response):
        print("Cookies Set...")

        for api_url in self.api_urls:
            url = self.config_url(api_url)
            request = scrapy.Request(
                url=url,
                method="GET",
                dont_filter=True,
                headers=self.headers,
                callback=self.parse_api,
            )
            request.cb_kwargs["api_url"] = api_url
            print(url)

            yield request

    def parse_api(self, response, api_url):
        raw_data = response.body
        data = json.loads(raw_data)
        jobs = data["jobs"]
        if len(jobs) == 0:
            print("No jobs fetched...")
            return

        spider_item = JobItem()
        for job in jobs:
            spider_item["source"] = "www.hirist.com"
            spider_item["title"] = job["title"]
            spider_item["company"] = job["companyData"]["companyName"]
            spider_item["posting_time"] = datetime.datetime.fromtimestamp(
                job["createdTimeMs"] / 1000.0
            )

            spider_item["apply_by"] = None
            spider_item["duration"] = None

            min = job["min"]
            max = job["max"]
            spider_item["experience"] = f"{min} - {max} years"

            location_list = job["location"]
            location_str = ""
            for location in location_list:
                location_str += location["name"] + ", "
            location_str = location_str.rstrip(" ")
            location_str = location_str.rstrip(",")
            spider_item["location"] = location_str

            spider_item["link"] = f"{self.start_urls[0]}j/{job['id']}"

            yield spider_item

        has_more = data["hasMore"]
        print("Parsed Page-", self.page_no)
        if has_more == True:
            self.page_no += 1
            url = self.config_url(api_url)
            request = scrapy.Request(
                url=url,
                method="GET",
                dont_filter=True,
                headers=self.headers,
                callback=self.parse_api,
            )
            request.cb_kwargs["api_url"] = api_url

            yield request
        else:
            print("last page: ", self.page_no)
            self.page_no = 0
            return
