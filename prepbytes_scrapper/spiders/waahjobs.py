from selenium import webdriver
import scrapy
from ..items import JobItem


class WaahjobsSpider(scrapy.Spider):
    name = "waahjobs"

    def start_requests(self):
        urls = [
            "https://www.waahjobs.com/s/software-developer-jobs-in-mumbai/",
            "https://www.waahjobs.com/s/software-developer-jobs-in-bangalore/",
            "https://www.waahjobs.com/s/software-developer-jobs-in-delhi/",
            "https://www.waahjobs.com/s/software-developer-jobs-in-pune/",
            "https://www.waahjobs.com/s/software-developer-jobs-in-kolkata/",
            "https://www.waahjobs.com/s/software-developer-jobs-in-gurgaon/",
            "https://www.waahjobs.com/s/software-developer-jobs-in-noida/",
        ]
        for url in urls:
            yield scrapy.Request(url=url, callback=self.parse)

    def parse(self, response):
        item = response.css(".r-95jzfe .css-1dbjc4n .r-1pn2ns4")

        for i in range(0, len(item)):
            ref = item[i].css("a.alink").attrib["href"]
            link = "https://www.waahjobs.com" + ref
            request = response.follow(link, callback=self.parse_job_page)

            yield request

    def parse_job_page(self, response):
        spider_item = JobItem()
        spider_item["source"] = "www.waahjobs.com"
        spider_item["title"] = response.css(".r-13awgt0 .r-vrz42v::text").get()
        spider_item["company"] = response.css(".alink .r-1it3c9n.r-rjixqe::text").get()
        spider_item["posting_time"] = None
        spider_item["apply_by"] = None
        spider_item["duration"] = None

        card = response.css(".r-1ybube5").xpath(".//div[@dir='auto']/text()").getall()

        data_index = {"salary": -1, "experience": -1}
        for i in range(0, len(card)):
            if "experience" in card[i].lower():
                data_index["experience"] = i
            if "salary" in card[i].lower():
                data_index["salary"] = i

        if data_index["experience"] > 2:
            spider_item["location"] = f"{card[0]}, {card[1]}"
        else:
            spider_item["location"] = None

        spider_item["experience"] = card[data_index["experience"] - 1]

        salary_amount = card[data_index["salary"] - 1]
        salary_period = card[data_index["salary"]]
        spider_item["salary"] = f"{salary_amount} ({salary_period})"

        description = response.css("#org_read_more").css("p::text").getall()
        description_text = ""
        for i in range(0, len(description)):
            description_text += description[i]
        spider_item["description_text"] = description_text

        spider_item["link"] = response.url

        yield spider_item
