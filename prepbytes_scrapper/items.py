# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy


class JobItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    source = scrapy.Field()
    title = scrapy.Field()
    company = scrapy.Field()
    posting_time = scrapy.Field()
    apply_by = scrapy.Field()
    duration = scrapy.Field()
    experience = scrapy.Field()
    salary = scrapy.Field()
    location = scrapy.Field()
    link = scrapy.Field()
    description = scrapy.Field()
    description_text = scrapy.Field()
    pass
